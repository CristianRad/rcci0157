package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTest {

    private Employee e1;
    private EmployeeMock employeeMock;
    private List<Employee> employeeList;

    @Before
    public void setUp() {
        e1 = new Employee();
        e1.setId(3);
        e1.setFirstName("Ionut");
        e1.setLastName("Popa");
        e1.setCnp("1840523124948");
        e1.setSalary(3400d);
        e1.setFunction(DidacticFunction.TEACHER);

        employeeMock = new EmployeeMock();
        employeeList = new ArrayList<>();
    }

    @After
    public void tearDown() {
        e1 = null;
        employeeMock = null;
    }

    @Test(expected = NullPointerException.class)
    public void testModifyEmployeeFunction1() {
        List <Employee> initialList = employeeMock.getEmployeeList();
        e1 = null;
        employeeList.add(e1);
        employeeMock.modifyEmployeeFunction(employeeMock.findEmployeeById(e1.getId()), DidacticFunction.CONFERENTIAR);
        List <Employee> modifiedList = employeeMock.getEmployeeList();
        assertEquals(0, initialList.equals(modifiedList));
    }

    @Test
    public void testModifyEmployeeFunction2() {
        employeeMock.modifyEmployeeFunction(employeeMock.findEmployeeById(e1.getId()), DidacticFunction.CONFERENTIAR);
        assertEquals(DidacticFunction.CONFERENTIAR, employeeMock.findEmployeeById(e1.getId()).getFunction());
    }

}