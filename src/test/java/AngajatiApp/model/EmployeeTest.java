package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.repository.EmployeeMock;
import org.junit.*;

import static org.junit.Assert.*;

public class EmployeeTest {

    private Employee e1;
    private Employee e2;
    private Employee e3;
    private Employee e4;
    private Employee e5;
    private Employee e6;
    private EmployeeMock newEmployee1;
    private EmployeeMock newEmployee2;

    @Before
    public void setUp() {
        e1 = new Employee();
        e1.setFirstName("Ionut");
        e1.setId(1);

        e2 = new Employee();
        e2.setFirstName("Bogdan");

        e3 = new Employee();

        e4 = null;

        e5 = new Employee();
        e5.setFirstName("Ionut");
        e5.setLastName("Pop");
        e5.setFunction(DidacticFunction.TEACHER);
        e5.setCnp("1910314124938");
        e5.setSalary(3500.0);

        e6 = new Employee();
        e6.setFirstName("Mircea");
        e6.setLastName("Irimie");
        e6.setFunction(DidacticFunction.LECTURER);
        e6.setCnp("1840918124945");
        e6.setSalary(6200.0);

        newEmployee1 = new EmployeeMock();
        newEmployee2 = new EmployeeMock();

        System.out.println("Before test");
    }

    @After
    public void tearDown() {
        e1 = null;
        e2 = null;

        System.out.println("After test");
    }

    @BeforeClass
    public static void setUpAll() {
        System.out.println("Before All Tests - Beginning of Class");
    }

    @AfterClass
    public static void tearDownAll() {
        System.out.println("After All Tests - End of Class");
    }

    @Test
    public void testGetFirstName() {
        assertEquals("Ionut", e1.getFirstName());
    }

    @Test (expected = NullPointerException.class)
    public void testGetFirstName2() {
        assertEquals("Bogdan", e4.getFirstName());
    }

    @Test
    public void testSetCnp() {
        e3.setCnp("1940130124936");
        assertEquals("1940130124936", e3.getCnp());
    }

    @Ignore
    @Test
    public void testGetId() {
        assertEquals(1, e1.getId());
    }

    @Test
    public void testConstructor() {
        assertNotEquals("S-a creat angajatul e3", e3, null);
    }

    @Test (timeout = 200)
    public void testFictiv() {
        try {
            Thread.sleep(190);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testAddEmployee1() { //ECP 1 + BVA 2
        assertTrue("Employee was added", newEmployee1.addEmployee(e5));
    }

    @Test
    public void testAddEmployee2() { //ECP 2 + BVA 1
        e5.setCnp("191031412493");
        assertFalse("Employee was added", newEmployee1.addEmployee(e5));
    }

    @Test
    public void testAddEmployee3() { //BVA 3
        e5.setCnp("19103141249380");
        assertFalse("Employee was not added", newEmployee1.addEmployee(e5));
    }

    @Test
    public void testAddEmployee4() { //ECP 3 + BVA 4
        e5.setFirstName("");
        e5.setLastName("");
        assertFalse("Employee was added", newEmployee1.addEmployee(e5));
    }

    @Test
    public void testAddEmployee5() { //BVA 6
        assertTrue("Employee was added", newEmployee1.addEmployee(e5));
        assertTrue("Employee was added", newEmployee2.addEmployee(e6));
    }

}