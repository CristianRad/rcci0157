package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;

import java.util.Objects;

public class Employee {

    private int id;
    private String lastName;
    private String firstName;
    private String cnp;
    private DidacticFunction function;
    private Double salary;

    public Employee() {
    }

    public Employee(int id, String firstName, String lastName, String cnp, DidacticFunction function, Double salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.cnp = cnp;
        this.function = function;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public DidacticFunction getFunction() {
        return function;
    }

    public void setFunction(DidacticFunction function) {
        this.function = function;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", cnp='" + cnp + '\'' +
                ", function=" + function +
                ", salary=" + salary +
                '}';
    }

    @Override
    public boolean equals(Object otherEmployee) {
        if (otherEmployee == this) { return true; }
        if (!(otherEmployee instanceof Employee)) { return false; }

        final Employee employee = (Employee) otherEmployee;

        boolean hasSameId = this.id == employee.getId(),
                hasSameFirstName = this.firstName.equals(employee.getFirstName()),
                hasSameLastName = this.lastName.equals(employee.getLastName()),
                hasSameCNP = this.cnp.equals(employee.getCnp()),
                hasSameFunction = this.function.equals(employee.getFunction()),
                hasSameSalary = this.salary.equals(employee.getSalary());
        return hasSameId && hasSameFirstName && hasSameLastName && hasSameCNP && hasSameFunction && hasSameSalary;
    }

    @Override
    public int hashCode() { return Objects.hash(id, firstName, lastName, cnp, function, salary); }

}
