package AngajatiApp.repository;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import AngajatiApp.model.AgeCriteria;
import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import AngajatiApp.validator.EmployeeValidator;
import AngajatiApp.model.SalaryCriteria;

public class EmployeeImpl implements EmployeeRepositoryInterface {

	private static final int LAST_NAME_INDEX = 1;
	private static final int FIRST_NAME_INDEX = 0;
	private static final int CNP_INDEX = 2;
	private static final int DIDACTIC_FUNCTION_INDEX = 3;
	private static final int SALARY_INDEX = 4;
	private static final int ID = 5;

	private static final String ERROR_WHILE_READING_MSG = "Error while reading: ";
	private final String employeeDBFile = "employeeDB/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();
	private List<Employee> employeeList = new ArrayList<>();
	
	public EmployeeImpl() {
		employeeList = loadEmployeesFromFile();
	}

	@Override
	public boolean addEmployee(Employee employee) {
		employee.setId(employeeList.size());
		if (employeeValidator.isValid(employee)) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				employeeList.add(employee);
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void modifyEmployeeFunction(Employee oldEmployee, DidacticFunction newFunction) {
		oldEmployee.setFunction(newFunction);
	}
	
	private List<Employee> loadEmployeesFromFile() {
		final List<Employee> employeeList = new ArrayList<Employee>();
		try (BufferedReader br = new BufferedReader(new FileReader(employeeDBFile))) {
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				try {
					final Employee employee = getEmployeeFromString(line, counter);
					employeeList.add(employee);
					//counter++;
				} catch (EmployeeException ex) {
					System.err.println(ERROR_WHILE_READING_MSG + ex.toString());
				}
			}
		} catch (IOException e) {
			System.err.println(ERROR_WHILE_READING_MSG + e);
		} 
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeByCriteria() {
		List<Employee> employeeSortedList = new ArrayList<Employee>(employeeList);
		Collections.copy(employeeSortedList, employeeList);
		Collections.sort(employeeSortedList, new AgeCriteria());
		//System.out.println(employeeSortedList);
		Collections.sort(employeeSortedList, new SalaryCriteria());
		//System.out.println(employeeSortedList);
		return employeeSortedList;
	}

	@Override
	public Employee findEmployeeById(int idOldEmployee) {
		for (Employee employee : employeeList) {
			if (employee.getId() == idOldEmployee) {
				return employee;
			}
		}
		return null;
	}

	@Override
	public Employee deleteEmployeeById(int idOldEmployee) {
		Employee employeeToRemove = null;
		for (Employee employee : employeeList) {
			if (employee.getId() == idOldEmployee) {
				employeeToRemove = employee;
				employeeList.remove(idOldEmployee);
			}
		}
		return employeeToRemove;
	}

	/**
	 * Returns the Employee after parsing the given line
	 *
	 * @param employeeString the employee given as String from the input file
	 * @param line           the current line in the file
	 * @return if the given line is valid returns the corresponding Employee
	 * @throws EmployeeException
	 */
	public static Employee getEmployeeFromString(String employeeString, int line) throws EmployeeException {
		Employee employee = new Employee();

		String[] attributes = employeeString.split("[;]");

		if (attributes.length != 6) {
			throw new EmployeeException("Invalid line at: " + line);
		} else {
			EmployeeValidator validator = new EmployeeValidator();
			employee.setId(Integer.parseInt(attributes[ID]));
			employee.setFirstName(attributes[FIRST_NAME_INDEX]);
			employee.setLastName(attributes[LAST_NAME_INDEX]);
			employee.setCnp(attributes[CNP_INDEX]);

			if (attributes[DIDACTIC_FUNCTION_INDEX].equals("ASISTENT"))
				employee.setFunction(DidacticFunction.ASISTENT);
			if (attributes[DIDACTIC_FUNCTION_INDEX].equals("LECTURER"))
				employee.setFunction(DidacticFunction.LECTURER);
			if (attributes[DIDACTIC_FUNCTION_INDEX].equals("TEACHER"))
				employee.setFunction(DidacticFunction.TEACHER);
			if (attributes[DIDACTIC_FUNCTION_INDEX].equals("CONFERENTIAR"))
				employee.setFunction(DidacticFunction.CONFERENTIAR);

			employee.setSalary(Double.valueOf(attributes[SALARY_INDEX]));
			//employee.setId(Integer.valueOf(attributes[ID]));
			employee.setId(Integer.valueOf(line));

			if (!validator.isValid(employee)) {
				throw new EmployeeException("Invalid line at: " + line);
			}
		}
		return employee;
	}

}
