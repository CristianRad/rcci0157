package AngajatiApp.main;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeImpl;
//import repository.EmployeeMock;
import AngajatiApp.repository.EmployeeMock;
import AngajatiApp.repository.EmployeeRepositoryInterface;

import java.util.Scanner;

import AngajatiApp.controller.EmployeeController;

//functionalitati
//i.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//ii.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//iii.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
public class StartApp {

    private static Scanner scanner;

    public static void main(String[] args) {
        EmployeeMock employeesRepository = new EmployeeMock();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        scanner = new Scanner(System.in);
        while (true) {
            employeeController.printMenu();
            int command;
            try {
                command = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("Exit!");
                return;
            }
            switch (command) {
                case 1:
                    Employee employee = getEmployeeFromInput();
                    employeeController.addEmployee(employee);
                    System.out.println("Employee was added!");
                    break;
                case 2:
                    System.out.println("Employee ID: ");
                    int idOldEmployee = scanner.nextInt();
                    Employee oldEmployee = employeeController.findEmployeeById(idOldEmployee);
                    System.out.println("Employee function: ");
                    String newFunction = scanner.next();
                    System.out.println("Employee salary: ");
                    Double newSalary = scanner.nextDouble();
                    employeeController.modifyEmployee(oldEmployee, getDidacticFunction(newFunction));
                    break;
                case 3:
                    for (Employee employeeItem : employeeController.getEmployeesList()) {
                        System.out.println(employeeItem);
                    }
                    break;
                case 4:
                    System.out.println("Enter the ID of the employee to find: ");
                    int idToFind = scanner.nextInt();
                    Employee employeeToFind = employeeController.findEmployeeById(idToFind);
                    if (employeeToFind != null) { System.out.println("Employee found - " + employeeToFind); }
                    break;
                case 5:
                    System.out.println("Enter the Id of the employee to remove: ");
                    int idToRemove = scanner.nextInt();
                    Employee employeeToRemove = employeeController.deleteEmployeeById(idToRemove);
                    if (employeeToRemove != null) { System.out.println("Employee removed - " + employeeToRemove); }
                    break;
                default:
                    System.out.println("Exit!");
                    return;
            }
        }
    }

    private static Employee getEmployeeFromInput() {
        System.out.println("ID: ");
        int id = scanner.nextInt();
        System.out.println("First name: ");
        String firstName = scanner.next();
        System.out.println("Last name: ");
        String lastName = scanner.next();
        System.out.println("CNP: ");
        String cnp = scanner.next();
        System.out.println("Function: ");
        String didacticFuntion = scanner.next();
        System.out.println("Salary: ");
        Double salary = scanner.nextDouble();
        return new Employee(id, firstName, lastName, cnp, getDidacticFunction(didacticFuntion), salary);
    }

    private static DidacticFunction getDidacticFunction(String didacticFunction) {
        DidacticFunction function = null;
        if (didacticFunction.toUpperCase().equals("ASISTENT")) {
            function = DidacticFunction.ASISTENT;
        }
        if (didacticFunction.toUpperCase().equals("LECTURER")) {
            function = DidacticFunction.LECTURER;
        }
        if (didacticFunction.toUpperCase().equals("TEACHER")) {
            function = DidacticFunction.TEACHER;
        }
        if (didacticFunction.toUpperCase().equals("CONFERENTIAR")) {
            function = DidacticFunction.CONFERENTIAR;
        }
        return function;
    }

}
