package AngajatiApp.view;

public class EmployeeView {
	
	public void printMenu() {
		System.out.println("--- Menu ---");
		System.out.println("\t1. Add new employee");
		System.out.println("\t2. Modify didactic function and salary of existing employee");
		System.out.println("\t3. Show all employees");
		System.out.println("\t4. Find employee by id");
		System.out.println("\t5. Remove an employee");
		System.out.println("\t0. Exit");
	}

}
